import 'package:flutter/material.dart';

class AvatarScreen extends StatelessWidget {
   
  const AvatarScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('JOnathan teran'),
        actions: [
          Container(
            margin: const EdgeInsets.only(right: 3),
            child:  CircleAvatar(
              child:  const Text('JT'),
              backgroundColor: Colors.indigo[900]
            ),
          )
        ],

        
),
      body: const Center(
         child: CircleAvatar(
          maxRadius: 110,
          backgroundImage: NetworkImage('https://media-exp1.licdn.com/dms/image/C4E03AQHgo4ZnEjWy8Q/profile-displayphoto-shrink_800_800/0/1649862115159?e=1666828800&v=beta&t=b_igFlnRF6NFr-4nKLmt_B55pcGxsGhUH3h50xQs6vQ'),
         )
      ),
    );
  }
}