
import 'package:fl_components/theme/app_theme.dart';
import 'package:flutter/material.dart';


class CustomCardType1 extends StatelessWidget {
  const CustomCardType1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child:  Column(
        children: [
          const ListTile (
            leading: Icon(Icons.photo_album_outlined, color: AppTheme.primary),
            title: Text('Soy titulo'),
            subtitle: Text('Qui ex cillum id sit sint culpa occaecat dolore tempor consectetur. Consequat laboris deserunt culpa incididunt aliquip. Excepteur amet eu nostrud id nisi aliquip incididunt.'),
          ),
          Padding(
            padding: const EdgeInsets.only(right:5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: (){},
                  child: const Text('Cancel'),
                
                  ),
                  TextButton(
                  onPressed: (){},
                  child: const Text('Ok')
                  )
              ],
            ),
          )
        ],

      )
    );
  }
}