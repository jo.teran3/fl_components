import 'package:fl_components/widgets/custom_card_type_1.dart';
import 'package:flutter/material.dart';

import '../widgets/custom_card_type_2.dart';

class CardScreen extends StatelessWidget {

  const CardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Tarjetas Widget'),
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          children: const [
            CustomCardType1(),
            SizedBox(height: 10),
            CustomCardType2(imageUrl: 'https://www.eluniverso.com/resizer/cJWEDj9JCsJLYxSow3l6kdTtRtU=/895x670/smart/filters:quality(70)/cloudfront-us-east-1.images.arcpublishing.com/eluniverso/T6ATDRO62VDZXFQHRJXI4L6CI4.jpg',name: 'Malecon Gye'),
            SizedBox(height: 20),
            CustomCardType2(imageUrl: 'https://media.traveler.es/photos/6137777c7ad90bc43bae063a/master/w_1600%2Cc_limit/108543.jpg'),
            SizedBox(height: 20),
            CustomCardType2(imageUrl: 'https://content.r9cdn.net/rimg/dimg/6a/70/5c973b4c-city-20093-16561dd06c7.jpg?width=1366&height=768&xhint=1728&yhint=1044&crop=true', name: 'Cerro el faro',),
            SizedBox(height: 100),
          ],
        ));
  }
}